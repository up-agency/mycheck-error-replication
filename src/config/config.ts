const config = {
  "MycheckCreds": {
    "publishableKey": "PUBLISHABLE_KEY",
    "secretKey": "SECRET_KEY"
  }
}

export default config;
