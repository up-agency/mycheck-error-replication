import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import config from '../../config/custom-config';

// tslint:disable-next-line:no-any
declare const mycheckWallet: any;

const publishableKey = config.MycheckCreds.publishableKey;
const secretKey = config.MycheckCreds.secretKey;

interface WalletCard {
  credit_type: string;
  exp_month: string;
  exp_year4: string;
  source: string;
  token: string
}

@Component({
  selector: 'app-mycheck',
  templateUrl: './mycheck.component.html',
  styleUrls: ['./mycheck.component.sass']
})
export class MycheckComponent implements OnInit {
  public refreshToken: string;
  public isFormValid: boolean;

  constructor(
    public readonly http: HttpClient,
  ) { }

  public ngOnInit() {
    this.setupPayment();
    setTimeout(() => {
      this.setupWidget();
    }, 1000)
  }

  public setupWidget() {
    if (!mycheckWallet) {
      throw new Error('The Mycheck Wallet Checkout SDK script has not been loaded');
    }

    mycheckWallet.init('mywalletSdk', {
      refreshToken: this.refreshToken,
      publishableKey: publishableKey,
      acceptedCards: ['visa', 'mastercard', 'amex'],
      events: {
        // afterSelectCreditCard: (token: any) => console.log("new tok ", token),
        afterFormIsReady: (value: {isValid: boolean}) => {
          if (value.isValid) {
            this.isFormValid = true;
          } else {
            this.isFormValid = false;
          }
        }
        // after3DsComplete: (value: any) => console.log("new 3ds complete", value),
        // userData: (value: any) => console.log("ud", value)
      }
    });
  }

  public setupPayment() {
    const httpOptions = {
      headers: new HttpHeaders({
        'secretKey': secretKey
      })
    };

    const postData = {
      externalID: 'external_id'
    };

    this.http.post("https://the-sandbox.mycheckapp.com/users/api/v1/refresh_token", postData, httpOptions).subscribe((response: {
      status: string;
      refreshToken: string;
    }) => {
      if (response.status === 'OK') {
        this.refreshToken = response.refreshToken;
      }
    });
  }

}
