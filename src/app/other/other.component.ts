import { Component, OnInit, NgZone } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-other',
  templateUrl: './other.component.html',
  styleUrls: ['./other.component.sass']
})
export class OtherComponent implements OnInit {

  constructor(
    private readonly router: Router,
    private ngZone: NgZone) { }

  ngOnInit() {
    console.log('Other component');

  }

  public goToHome(event: Event) {
    event.preventDefault();
    this.ngZone.run(() => {
      this.router.navigate(['']);
    });
  }
}
