import { Component, OnInit, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import * as loadjs from 'load-js';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {
public hasPaymentScriptLoaded = false;
  constructor(
    private readonly router: Router,
    private ngZone: NgZone) { }

  ngOnInit() {
    console.log('Home component');
  }

  public ngAfterViewInit() {
    this._loadPaymentProviderScript();
  }

  private _loadPaymentProviderScript() {
    const scriptUrl = 'https://wallet-sdk-sandbox.mycheckapp.com/sdk.js';


    const comp = this;
    loadjs([scriptUrl]).then(() => {
      comp.hasPaymentScriptLoaded = true;
    });
  }

  public goToOther(event: Event) {
    event.preventDefault();
    this.ngZone.run(() => {
      this.router.navigate(['other'])
    });
  }

}
