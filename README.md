# MycheckReplication

An error occurs when trying to use the Mycheck SDK in an angular app.

It may come from trying to use an Angular and React app in the same application.

This application is built to replicate that error.

We encountered two errors:

1. When we try to insert the script in the index.html file

![alt text](error-imgs/script_in_index_error.png "Script in index error")

2. When we try to load the sdk.js file asynchronously with load-js (https://www.npmjs.com/package/load-js)

Here's a link to the React error:

https://reactjs.org/docs/error-decoder.html/?invariant=200

![alt text](error-imgs/react_error.png "React error")

## Setup

Run `npm install` to install node packages

## Change Config Variables

Enter you own config variables in `src/congig/config.ts` files

Change the file name of `config.ts` to `custom-config.ts`

## Launch

Use `npm start` to launch
